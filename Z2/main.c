#include <stdio.h>          // standart lib
#include <stdbool.h>        // bool lib

#define END 99.0            //
#define ZERO 0.0            //  -->             ***** Global Values *****
#define MAX_INPUT  10.0     //  --> END -> program endpoint, 
#define MIN_INPUT -10.0     //      MAX and MIN -> restriction of the incoming stream

int main() {
    FILE *file;     //  Variable of type File

    double value, old_value;    //  Variables for the incoming stream

    int error_counter = 0,                  //
        value_counter = 0,                  //  --> Counters values
        total_value_counter = 0;            //
    
    int intersection_counter = 0;
        //total_intersection_counter = 0;
    
    int index = 0;

    // *** Fn fopen(arg1, arg2) ***
    //
    // arg1 -> path to file
    // arg2 -> file working conditions
    //
    file = fopen("./data/dane1.txt", "r");
    // If the path is specified incorrectly
    // prog "return error"
    if (file == NULL)
        printf("Can't open for reading.\n");

    else {
        // *** "main" loop ***
        //
        // Will be executed until we get the final value: 99
        //
        while (value != END) {
            index++; // <- this value necessary for dividing the stream into parts of 99 elments
            // *** Fn fscanf(arg1, arg2, arg3) ***
            //
            // arg1 -> variable of file
            // arg2 -> type of value: %lf --> double
            // arg3 -> variables for the incoming stream
            fscanf(file, "%lf", &value);    
            //printf("%.2lf ", value);
            
            // The first check of the value,
            // if it cross the limit, an error
            // will be counted
            if (value < MAX_INPUT && value > MIN_INPUT) {
                value_counter++;
                // "Mirror" intersection check
                if ((value > ZERO && old_value < ZERO) || (value < ZERO && old_value > ZERO))
                    intersection_counter++;
                // Check for two zeros in row
                if ((value == ZERO && old_value != ZERO))
                    intersection_counter++;
                // Remember curient value in old_value
                old_value = value;
                //printf("%.2lf ", old_value);

            // Count error
            } else {

                error_counter++;

            }
            // Dividing the stream into parts
            if (index == 100) {
                index = 0;
                // Final check
                if (intersection_counter > 8 && intersection_counter < 14) {
                    printf("Sorry, you're dying\n");
                }

                //total_intersection_counter += intersection_counter;
                intersection_counter = 0;

            }

            total_value_counter++;

        }

        fclose(file);

        printf("\n\nValue counter: %d\n", value_counter);
        printf("Total value counter: %d\n", total_value_counter);   // For dane2 997-> check line 294 295 1000
        printf("Out of range: %d\n", error_counter);
        //printf("Total Intersection Zero: %d\n", total_intersection_counter);
        
    }

    return 0;

}

// ***                   *** TEST 1 ***                     ***
// ---         if the user specified the wrong path         ---
// --> program return error mesage: Can't open for reading. <--

// ***                   *** TEST 2 ***                     ***
// --- if all values are greater then the limit[-11, 14 ...]---
// -->         none of the values will be processed         <--

// ***                   *** TEST 3 ***                     ***
// ---          if first value will be a 99                 ---
// -->     program will be closed after the first loop      <--