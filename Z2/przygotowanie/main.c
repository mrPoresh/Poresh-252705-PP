#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#define END 99

void PrintOptions();

int main() {

    bool status = true;
    int option, temp, num, num_counter;

    do {

        PrintOptions();
        scanf("%d", &option);

        switch (option) {

            case 1:

                for (int i = 1, counter = 0; i < 100; i++) {

                    if (counter == 10) {

                        printf("\n %d ", i);

                        counter = 0;

                    } else {
                        
                        printf(" %d ", i);
                        
                    }

                    counter++;

                }

                break;

            case 2: 

                printf("Give me temp in Wro\n");
                scanf("%d", &temp);

                if (temp > 0) {

                    if (temp > 0 && temp < 11) {

                        printf("So cold\n");

                    } else if (temp > 10 && temp < 21) {

                        printf("Warm\n");

                    } else {

                        printf("Hot!");

                    }

                } else {

                    if (temp < 0 && temp > -11) {

                        printf("Cold\n");

                    } else if (temp < -10 && temp > -21) {

                        printf("Very cold\n");

                    } else {

                        printf("Death .____'\n");

                    }


                }

                break;

            case 3:

                num_counter = 0;

                while(true) {

                    num_counter++;

                    printf("Give me num in [-5...5]\n");
                    scanf("%d", &num);

                    if (num > -6 && num < 6) {

                        printf("Series %d. Value: %d\n"
                               "Correct value! %d   \n", num_counter, num, num);

                    } else if (num < END && num > -END) {

                        printf("Series %d. Value: %d\n"
                               "Error value!        \n", num_counter, num);

                    } else {

                        break;

                    }

                }

                break;

            case 4:

                status = false;

                break;

        }


    } while (status == true);

}



void PrintOptions() {

    printf("\n1. 1 by 99\n"
        "2. temp   \n"
        "3. num    \n"
        "4. exit   \n");

}


// #define PGWE +some
// #define PDWE -some
//
//  | y
//  |
//  |-------------------- PGWE
//  |
//  |       .
//  |   . .     .
//  |_.__________.________ x 
// 0|               .
//  |                 .
//  |------------------- PDWE
//  |
//