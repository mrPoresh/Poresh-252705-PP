import matplotlib.pyplot as plt
import numpy as np


def main() :

    print("Enter Path to file:")
    path = str(input()) # Default  '../data/input2.txt'

    print("Set amount lines:") # Default 1010
    count = int(input())

    data = handling_data(path, count)
    print("* All Clean Data *\n",data)
    print("* Amount elements ---> ", np.count_nonzero(data))

    time, values = split_array(data)

    create_plot(time, values)

###------------------------### Handling Data ###-------------------------###

def handling_data(path: str, count_lines: int) :
    """
    * This fn for Read, Write and Handling input Data.                    *
    
    * arg1(path) -> Path to Data file                                     *
    * arg2(count_lines) -> Amout iterations for lines in Data file        *

    * return -> Numpy array with clean Data, without 'n' and white spaces *
    
    """

    lines = []  # Empty list
    
    # Some Error Handling, in default fn try read file 
    try :
        with open(path) as file :   # Default reding from path
            for _ in range(0, count_lines) :   # Loop for lines                                                          
                # At each iteration the string is written to rhe list
                lines.append(str(file.readline()).replace('\n', ''))

            # (Some info) Print all amount items in list with []
            print("* Len with [] ---> ",len(lines))

            # Creating Numpy array without white spaces
            data = np.array([x for x in lines if x])
            print("* Len without [] > ", len(data))  
            # Array looking like:
            # ---> ["time_value" "time_value" ...] 

            return data

    # If something go wrong
    except :
        print("* Wrong path *")

    # And always
    finally :
        file.close()

###----------------------------------------------------------------------###

###---------------------### Splitting Arrays ###-------------------------###

def split_array(data: np.array) :
    """
    * This fn for Split a arrays in two with a space beetwen the data.    *

    * arg(np.array) -> Our Data array                                     *

    * return -> 2 arrays with time and values                             *

    """
    # The first values(time) of each pair
    time = np.array([x.split()[0] for x in data])
    print("* Only time --->\n", time)
    print("* Amount elements ---> ", np.count_nonzero(time))

    # The second values(time) of each pair
    values = np.array([x.split()[1] for x in data]).astype(np.float64)
    print("* Only Values --->\n", values)
    print("* Amount elements ---> ", len(values))

    # Input Data ---> ["time_value" "time_value" ...]
    #
    # Output first array ---> ["time" "time" ...]
    # Output second array --> ["value" "value"..]

    return time, values

###----------------------------------------------------------------------###

###--------------------------### Drawing ###-----------------------------###

def create_plot(time: np.array, values: np.array) :

    fig, ax = plt.subplots()
    ax.plot(time, values) # Ours arrays

    ax.set(xlabel = 'time', ylabel = 'values') # Create asix
    ax.grid()

    #fig.savefig("..data/test.png") # Saving plot in data folder
    plt.show()

###----------------------------------------------------------------------###


if __name__ == "__main__":
    main()