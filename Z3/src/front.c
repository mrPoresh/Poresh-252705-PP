#include <stdio.h>


void help_print() {

    printf("*--------------* Help Table *--------------*\n"
           "|                                          |\n"
           "| Flags:                                   |\n"
           "| - n negative                             |\n"
           "| - b blure                                |\n"
           "| - c change level                         |\n"
           "| - g gamma                                |\n"
           "| - d draw                                 |\n"
           "| - t threshold                            |\n"
           "| - k kontur                               |\n"
           "| - e normalize                            |\n"
           "| - p parh                                 |\n"
           "|                                          |\n"
           "| For strat:                               |\n"
           "| some                                     |\n"
           "|                                          |\n"
           "*------------------------------------------*\n");

}

void threshold_print() {

    printf("*-----------* Threshold Table *------------*\n"
           "|                                          |\n"
           "| Options:                                 |\n"
           "| - d default                              |\n"
           "| - b black                                |\n"
           "| - w white                                |\n"
           "|                                          |\n"
           "*------------------------------------------*\n");

}