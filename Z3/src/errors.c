#include <stdio.h>
#include <stdlib.h>

#define MAX_FLAG 4
#define MIN_FLAG 2
#define HELP_VAL 1

#define LINE_LEN 1024


int validate_flags(int argc, char *argv[]) {
    if (argc > MAX_FLAG || argc < MIN_FLAG) {
        fprintf(stderr, "Error: Incoerect Flags amount\n");
        exit(-1);
    };

    if ((argc == MIN_FLAG && *argv[HELP_VAL] != 'h') ||
        (argc == MAX_FLAG && *argv[HELP_VAL] == 'h')) {
        fprintf(stderr, "Error: Alone flag must be h\n");
        exit(-1);
    };

    if (*argv[HELP_VAL] != 'h' && *argv[HELP_VAL] != 'n' &&
        *argv[HELP_VAL] != 'b' && *argv[HELP_VAL] != 'p' &&
        *argv[HELP_VAL] != 'g' && *argv[HELP_VAL] != 'c' &&
        *argv[HELP_VAL] != 'd' && *argv[HELP_VAL] != 't' &&
        *argv[HELP_VAL] != 'k' && *argv[HELP_VAL] != 'e') {

        fprintf(stderr, "Error: Incorect Flag\n");
        exit(-1);
    }

    //printf("? Validate flags OK\n");

    return 1;

}

int validate_choise(char choise) {
    if (choise != 'h' && choise != 'n' &&
        choise != 'b' && choise != 'p' &&
        choise != 'g' && choise != 'c' && 
        choise != 'q' && choise != 'd' &&
        choise != 't' && choise != 'k' &&
        choise != 'e') {

            fprintf(stderr, "Error: Incorect Flag\n");
            choise = 'h';

        }

    //printf("? Validate choise OK: %c\n", choise);

    return 1;

}

int validate_head(FILE *file, char *buffer) {
    if (file == NULL) {
        fprintf(stderr, "Error: File does not exist\n");
        exit(-1);
    }

    if (fgets(buffer, LINE_LEN, file) == NULL) {
        fprintf(stderr, "Error: Is not PGM\n");
        exit(-1);
    }

    if ((buffer[0] != 'P') || (buffer[1] != '2')) {
        fprintf(stderr,"Error: Is not PGM\n");
        exit(-1);
    }

    return 1;

}

int validate_comments(FILE *file, char *buffer) {
    int value, end = 0;

    do {
        if ((value=fgetc(file))=='#') {
            if (fgets(buffer, LINE_LEN, file)==NULL)
                end = 1;
        } else {
            ungetc(value,file);
        }

    } while (value == '#' && !end);

    return 1;
    
}