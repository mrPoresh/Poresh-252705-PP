#include <stdio.h>
#include <stdbool.h>

#define SIZE 255

void create_tab(int, int);      //  tab 100x100 and fill in tab arg1 * arg2 elments
void printf_tab(int, int);      //  printf tab while index_value != 0
void change_tab(int, int);      //  change values arg1 * arg2

void printf_opt();              //  print options
//int  ranges_err(int, int);    //  "errors handling"

int TAB[SIZE][SIZE];


int main() {

    bool status = true;
    int option, arg1, arg2;

    do {

        printf_opt();
        scanf("%d", &option);
        //printf("\n--- Debug Option -> %d ---\n", option);

        switch(option) {

            case 1:

                printf("\nGive 2 values(int)\n");
                scanf("%d %d", &arg1, &arg2);
                //printf("\n--- Debug Case 1 Args -> %d %d---\n", arg1, arg2);

                create_tab(arg1, arg2);

                break;

            case 2:

                printf("\nGive 2 values(int)\n");
                scanf("%d %d", &arg1, &arg2);
                //printf("\n--- Debug Case 2 Args -> %d %d---\n", arg1, arg2);

                printf_tab(arg1, arg2);

                break;

            case 3:

                printf("\nGive 2 values(int)\n");
                scanf("%d %d", &arg1, &arg2);
                //printf("\n--- Debug Case 3 Args -> %d %d---\n", arg1, arg2);

                change_tab(arg1, arg2);

                break;

            case 4:

                status = false;

                break;

            default:

                break;

        }

    } while(status == true);

}

void create_tab(int column, int line) {
    // ranges_err(column, line);

    if (column > SIZE || line > SIZE)
        printf("Counter out of range, values will be %d %d\n", column = 10, line = 10);

    for (int i = 0; i <= column; i++) {

        for (int j = 0; j <= line; j++) {

            TAB[i][j] = i + j;

        }

    }

}

void printf_tab(int column, int line) {
    // (column, line) = ranges_err(column, line);

    if (column > SIZE || line > SIZE)
        printf("Counter out of range, values will be %d %d\n", column = 10, line = 10);

    for (int i = 0; i <= column; i++) {

        for (int j = 0; j <= line; j++) {

            printf("%d\t", TAB[i][j]);

        }

        printf("\n");

    }

}

void change_tab(int column, int line) {
    // ranges_err(column, line);

    if (column > SIZE || line > SIZE)
        printf("Counter out of range, values will be %d %d\n", column = 10, line = 10);

    for (int i = 0; i <= column; i++) {

        for (int j = 0; j <= line; j++) {

            if (TAB[i][j] != 0)
                TAB[i][j] = -TAB[i][j];

        }

    }

    printf_tab(column, line);

}

//int ranges_err(int column, int line) {
//    int result[2];
//
//    if (column > 10 || line > 10)
//        printf("Counter out of range, values will be %d %d\n", column = 10, line = 10);
//
//    return column, line   ???????????????
//
//}

void printf_opt() {

    printf("\n1. Create Tab\n"
            "2. Print Tab  \n"
            "3. Change Tab \n"
            "4. Exit       \n"
            "Your choise -->\n");

}