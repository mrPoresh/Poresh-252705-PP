#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "./src/front.c"
#include "./src/errors.c"

#define LINE_LEN 1024
#define MAX_COLOR 255
#define MIN_COLOR 0

/*-------------------------------------------------* Struct File Info *----------------------------------------------------*/

struct FileInfo {

    char *in_path, *out_path;
    char *buffer;
    int  *data_pointer;
    int   size_x, size_y, grey_value;

};

struct FileInfo* create_fileInfo(char *in_path, char *out_path) {
    struct FileInfo* fileInfo = (struct FileInfo*)malloc(sizeof(struct FileInfo));

    fileInfo -> in_path = in_path;
    fileInfo -> out_path = out_path;

    return fileInfo;

}

void alloc_buffer(struct FileInfo *fileInfo, int size) {
    fileInfo -> buffer = (char* )calloc(size, sizeof(char));
}

void alloc_data(struct FileInfo *fileInfo, int size_x, int size_y, int grey_value) {
    fileInfo->data_pointer = (int *)malloc((size_x * size_y) * sizeof(int));

    fileInfo -> size_x = size_x;
    fileInfo -> size_y = size_y;
    fileInfo -> grey_value = grey_value;
}

void write_data(struct FileInfo *fileInfo, FILE *file) {
    for (int i = 0; i < fileInfo->size_x * fileInfo->size_y; i++) {
        fscanf(file, "%d", &fileInfo->data_pointer[i]);
    }
}

void destroy_fileInfo(struct FileInfo *fileInfo) {
    free(fileInfo -> data_pointer);
    free(fileInfo -> buffer);
    free(fileInfo);
}

void write_toFile(struct FileInfo *fileInfo) {
    FILE *file = fopen(fileInfo->out_path, "w");

    fputs("P2\n", file);
    fprintf(file, "%d %d\n%d\n", fileInfo->size_x, fileInfo->size_y, fileInfo->grey_value);

    for (int i = 0; i < fileInfo->size_x * fileInfo->size_y; i++) {
        fprintf(file, "%d ", fileInfo->data_pointer[i]);
    }

    fclose(file);
}

void draw(char *path) {
    char buf[LINE_LEN];

    strcpy(buf, "feh ");
    strcat(buf, path);
    strcat(buf, " &");
    printf("%s\n", buf);
    system(buf);
}

/*-------------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------------*/

void negative(struct FileInfo *fileInfo) {
    for (int i = 0; i < fileInfo->size_x * fileInfo->size_y; i++) {
        fileInfo->data_pointer[i] = MAX_COLOR - fileInfo->data_pointer[i];
    }
}

void threshold_default(struct FileInfo *fileInfo, int value) {
    value = MAX_COLOR * (value * 0.01);

    for (int i = 0; i < fileInfo->size_x * fileInfo->size_y; i++) {
        if (fileInfo->data_pointer[i] <= value) {
            fileInfo->data_pointer[i] = MIN_COLOR;

        } else if (fileInfo->data_pointer[i] > value) {
            fileInfo->data_pointer[i] = MAX_COLOR;

        } else {
            fprintf(stderr, "Error: Can not convent\n");
            exit(-1);
        }
    }
}

void threshold_black(struct FileInfo *fileInfo, int value) {
    value = MAX_COLOR * (value * 0.01);

    for (int i = 0; i < fileInfo->size_x * fileInfo->size_y; i++) {
        if (fileInfo->data_pointer[i] <= value) {
            fileInfo->data_pointer[i] = MIN_COLOR;

        } else if (fileInfo->data_pointer[i] > value) {
            fileInfo->data_pointer[i] = fileInfo->data_pointer[i];

        } else {
            fprintf(stderr, "Error: Can not convent\n");
            exit(-1);
        }
    }
}

void threshold_white(struct FileInfo *fileInfo, int value) {
    value = MAX_COLOR * (value * 0.01);

    for (int i = 0; i < fileInfo->size_x * fileInfo->size_y; i++) {
        if (fileInfo->data_pointer[i] <= value) {
            fileInfo->data_pointer[i] = fileInfo->data_pointer[i];

        } else if (fileInfo->data_pointer[i] > value) {
            fileInfo->data_pointer[i] = MAX_COLOR;

        } else {
            fprintf(stderr, "Error: Can not convent\n");
            exit(-1);
        }
    }
}

void c_gamma(struct FileInfo *fileInfo, double value) {
    for (int i = 0; i < fileInfo->size_x * fileInfo->size_y; i++) {
        fileInfo->data_pointer[i] = (int)
            (pow(fileInfo->data_pointer[i], (1.0 / value)) / pow(MAX_COLOR, ((1.0 - value) / value)));
    }
}

void change_level(struct FileInfo *fileInfo, int white, int black) {
    white = MAX_COLOR * (white * 0.01);
    black = MAX_COLOR * (black * 0.01);

    for (int i = 0; i < fileInfo->size_x * fileInfo->size_y; i++) {
        if (fileInfo->data_pointer[i] <= black) {
            fileInfo->data_pointer[i] = MIN_COLOR;

        } else if (fileInfo->data_pointer[i] > black && fileInfo->data_pointer[i] < white) {
            fileInfo->data_pointer[i] = (int)
                ((fileInfo->data_pointer[i] - black) * (MAX_COLOR / (white - black)));

        } else if (fileInfo->data_pointer[i] >= white) {
            fileInfo->data_pointer[i] = MAX_COLOR;
        
        } else {
            fprintf(stderr, "Error: Can not convent\n");
            exit(-1);
        }
    }
}

void blur(struct FileInfo *fileInfo, int value) {
    int size_y = fileInfo->size_x;
    int size_x = fileInfo->size_y;
    int iter = 0;
    int *array[size_x];

    for (int i = 0; i < size_x; i++) {
        array[i] = (int*)malloc(size_y * sizeof(int));
    }

    for (int i = 0; i < size_x; i++) {
        for (int j = 0; j < size_y; j++) {
            array[i][j] = fileInfo->data_pointer[iter];

            if ((i > value && j > value) && (i < size_x - value && j < size_y - value)) {
                fileInfo->data_pointer[iter] = (int)
                    (1.0/3.0 * (array[i - value][j - value] + array[i][j] + array[i + value][j + value]));

            } else { 
                fileInfo->data_pointer[iter] = array[i][j]; 
            }

            iter++;
        }
    }

    for (int i = 0; i < size_x; i++) {
        free(array[i]);
    }
}

void kontur(struct FileInfo *fileInfo, int value) {
    int size_y = fileInfo->size_x;
    int size_x = fileInfo->size_y;
    int iter = 0;
    int *array[size_x];

    for (int i = 0; i < size_x; i++) {
        array[i] = (int*)malloc(size_y * sizeof(int));
    }

    for (int i = 0; i < size_x; i++) {
        for (int j = 0; j < size_y; j++) {
            array[i][j] = fileInfo->data_pointer[iter];

            iter++;
        }

    }

    iter = 0;

    for (int i = 0; i < size_x; i++) {
        for (int j = 0; j < size_y; j++) {
            if ((i > value && j > value) && (i < size_x - value && j < size_y - value)) {
                fileInfo->data_pointer[iter] = (int)
                    (abs(array[i + value][j] - array[i][j]) + abs(array[i][j + value] - array[i][j]));

            } else { 
                fileInfo->data_pointer[iter] = array[i][j]; 
            }

            iter++;
        }
    }

    for (int i = 0; i < size_x; i++) {
        free(array[i]);
    }
}

void normalize(struct FileInfo *fileInfo) {
    int max_color = 125, min_color = 125;

    for (int i = 0; i < fileInfo->size_x * fileInfo->size_y; i++) {
        if (fileInfo->data_pointer[i] > max_color)
            max_color = fileInfo->data_pointer[i];
        if (fileInfo->data_pointer[i] < min_color)
            min_color = fileInfo->data_pointer[i];
    }

    for (int i = 0; i < fileInfo->size_x * fileInfo->size_y; i++) {
        fileInfo->data_pointer[i] = (int)
            ((fileInfo->data_pointer[i] - min_color) * (max_color / (max_color - min_color)));
    }
}

/*-------------------------------------------------------------------------------------------------------------------------*/

/*-------------------------* Main Fn *-------------------------------------------------------------------------------------*/ 

int main(int argc, char *argv[]) {              //*    https://www.geeksforgeeks.org/command-line-arguments-in-c-cpp/     *//
    char *option, *in_path, *out_path;          //* argc -> the number of command line arguments (including name of prog) *//
    char choise;                                //* **argv or *argv[] -> list of command-line arguments *                 *//
    int size_x, size_y, grey_value;
    int value, black, white;
    double g_value;

    validate_flags(argc, argv);

    if (argc > MIN_FLAG) {
        option = argv[1];
        in_path = argv[2];
        out_path = argv[3];

    } else {

        help_print();
        exit(-1);

    };

    do {

        struct FileInfo *fileInfo = create_fileInfo(in_path, out_path);
        alloc_buffer(fileInfo, LINE_LEN);

        FILE *file = fopen(in_path, "r");
        validate_head(file, fileInfo -> buffer);
        validate_comments(file, fileInfo -> buffer);
        fscanf(file, "%d %d %d", &size_x, &size_y, &grey_value);

        alloc_data(fileInfo, size_x, size_y, grey_value);
        write_data(fileInfo, file);
        fclose(file);

        switch(*option) {

            case 'h':

                help_print();

                break;

            case 'd':
                
                break;

            case 'n':

                negative(fileInfo);

                break;

            case 't':

                threshold_print();
                printf("Option: ");
                scanf("%s", option);

                printf("Enter %%");
                scanf("%d", &value);

                switch(*option) {

                    case 'd':

                        threshold_default(fileInfo, value);

                        break;

                    case 'b':

                        threshold_black(fileInfo, value);

                        break;    

                    case 'w':

                        threshold_white(fileInfo, value);

                        break;                

                };

                break;

            case 'g':

                printf("Gamma Value: ");
                scanf("%lf", &g_value);

                c_gamma(fileInfo, g_value);

                break;

            case 'c':

                printf("Enter White Black%%");
                scanf("%d %d", &white, &black);

                change_level(fileInfo, white, black);

                break;

            case 'b':

                printf("Enter power for Blur: ");
                scanf("%d", &value);

                blur(fileInfo, value);

                break;

            case 'k':

                kontur(fileInfo, 1);

                break;

            case 'e':

                normalize(fileInfo);

                break;

            case 'p':

                printf("Enter new Path <sourse> <out>: ");
                scanf("%s %s", in_path, out_path);

                break;

            case 'q':

                break;

        };

        write_toFile(fileInfo);
        draw(fileInfo -> out_path);
        destroy_fileInfo(fileInfo);

        printf("Next Option: ");
        scanf("%s", &choise);

        validate_choise(choise);
        *option = choise;


    } while(*option != 'q');

    return 0;

}