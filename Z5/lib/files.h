#ifndef FILES_PGM_H
#define FILES_PGM_H

#include <../lib/struct.h>


FileStruct* init_pgm(FileStruct*);

int read(FILE*, FileStruct*);
int write(FILE*, FileStruct*);

#endif
