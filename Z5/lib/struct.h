#ifndef STRUCT_H
#define STRUCT_H

#define BUFF_SIZE 1024

#include <stdio.h>

typedef struct {

    int size_x, size_y, grey_value;
    char *buffer;
    void *t_data; 

} FileStruct;

int alloc_buff(FileStruct*);
int alloc_data(FILE*, FileStruct*);
int free_memory(FileStruct*);

int validate(FILE*, char*);
int validate_head(FILE*, char*);
int validate_comments(FILE*, char*);

#endif
