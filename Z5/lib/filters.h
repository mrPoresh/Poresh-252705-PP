#ifndef FILTERS_H
#define FILTERS_H

#include <../lib/struct.h>

#define MAX_COLOR 255
#define MIN_COLOR 0


int negatiwe(FileStruct *);
int c_gamma(FileStruct *, double);
int threshold_default(FileStruct *, double);
int threshold_black(FileStruct *, double);
int threshold_white(FileStruct *, double);

int kontur(FileStruct *, double);
int blur(FileStruct *, double);
int normalize(FileStruct *);

int convent(FileStruct *);

#endif
