#!/bin/bash

cd build

cmake ..
make && mv gluvi ../

cd ..

#./gluvi -i data/tracks.pgm -o data/tracksD.pgm -d       #|
#./gluvi -i data/snap.pgm -o data/snapD.pgm -d           #| -> Just for displaying
#./gluvi -i data/planes.pgm -o data/planesD.pgm -d       #|
#./gluvi -i data/baboon.pgm -o data/baboonD.pgm -d       #|

#./gluvi -i data/tracks.pgm -o data/tracksN.pgm -n       #|
#./gluvi -i data/snap.pgm -o data/snapN.pgm -n           #|  -> For negatiwe
#./gluvi -i data/planes.pgm -o data/planesN.pgm -n       #|
#./gluvi -i data/baboon.pgm -o data/baboonN.pgm -n       #|

#./gluvi -i data/tracks.pgm -o data/tracksTD.pgm -t d 50 #|
#./gluvi -i data/snap.pgm -o data/snapTD.pgm -t d 50     #|  -> For Threshold Default
#./gluvi -i data/planes.pgm -o data/planesTD.pgm -t d 50 #|
#./gluvi -i data/baboon.pgm -o data/baboonTD.pgm -t d 50 #|

#./gluvi -i data/tracks.pgm -o data/tracksTB.pgm -t b 10 #|
#./gluvi -i data/snap.pgm -o data/snapTB.pgm -t b 20     #|  -> For Threshold Black
#./gluvi -i data/planes.pgm -o data/planesTB.pgm -t b 50 #|
#./gluvi -i data/baboon.pgm -o data/baboonTB.pgm -t b 50 #|

#./gluvi -i data/tracks.pgm -o data/tracksTW.pgm -t w 10 #|
#./gluvi -i data/snap.pgm -o data/snapTW.pgm -t w 20     #|  -> For Threshold White
#./gluvi -i data/planes.pgm -o data/planesTW.pgm -t w 50 #|
#./gluvi -i data/baboon.pgm -o data/baboonTW.pgm -t w 50 #|

#./gluvi -i data/tracks.pgm -o data/tracksG.pgm -g 2.5   #|
#./gluvi -i data/snap.pgm -o data/snapG.pgm -g 2.5       #|  -> For Gamma
#./gluvi -i data/planes.pgm -o data/planesG.pgm -g 2.5   #|
#./gluvi -i data/baboon.pgm -o data/baboonG.pgm -g 2.5   #|

#./gluvi -i data/tracks.pgm -o data/tracksK.pgm -k 3     #|
#./gluvi -i data/snap.pgm -o data/snapK.pgm -k 3         #|  -> For kontur
#./gluvi -i data/planes.pgm -o data/planesK.pgm -k 3     #|
#./gluvi -i data/baboon.pgm -o data/baboonK.pgm -k 3     #|

#./gluvi -i data/tracks.pgm -o data/tracksB.pgm -b 3     #|
#./gluvi -i data/snap.pgm -o data/snapB.pgm -b 3         #|  -> For Blure
#./gluvi -i data/planes.pgm -o data/planesB.pgm -b 3     #|
#./gluvi -i data/baboon.pgm -o data/baboonB.pgm -b 3     #|

#./gluvi -i data/tracks.pgm -o data/tracksR.pgm -r       #|
#./gluvi -i data/snap.pgm -o data/snapR.pgm -r           #|  -> For Blure
#./gluvi -i data/planes.pgm -o data/planesR.pgm -r       #|
#./gluvi -i data/baboon.pgm -o data/baboonR.pgm -r       #|

./gluvi -i data/star.ppm -o data/starD.ppm -d           #| -> Just disply ppm
./gluvi -i data/snail.ppm -o data/snailD.ppm -d         #|

#./gluvi -i data/star.ppm -o data/starN.ppm -n           #| -> For Negative
#./gluvi -i data/snail.ppm -o data/snailN.ppm -n         #|

./gluvi -i data/star.ppm -o data/starC.pgm -c           #| -> For Negative
./gluvi -i data/snail.ppm -o data/snailC.pgm -c         #|