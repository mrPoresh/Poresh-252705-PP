#include <../lib/filters.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int negatiwe(FileStruct *fileStruct) {
    int (*data)[fileStruct -> size_x];
    data = (int(*)[fileStruct -> size_x]) fileStruct -> t_data;

    for (int i = 0; i < fileStruct -> size_y; i++) {
        for (int j = 0; j < fileStruct -> size_x; j++) {
            data[i][j] = MAX_COLOR - data[i][j];
        }
    }
  
    return 0; 
}

int threshold_default(FileStruct *fileStruct, double value) {
    int (*data)[fileStruct -> size_x];
    data = (int(*)[fileStruct -> size_x]) fileStruct -> t_data;
    value = MAX_COLOR * (value * 0.01);
    
    for (int i = 0; i < fileStruct -> size_y; i++) {
        for (int j = 0; j < fileStruct -> size_x; j++) {
            if (data[i][j] <= value) {
                data[i][j] = MIN_COLOR;

            } else if (data[i][j] > value) {
                data[i][j] = MAX_COLOR;

            } else {
                fprintf(stderr, "Error: Can not convent\n");
                exit(-1);
            }
        }
    }

    return 0;    
}

int threshold_black(FileStruct *fileStruct, double value) {
    int (*data)[fileStruct -> size_x];
    data = (int(*)[fileStruct -> size_x]) fileStruct -> t_data;
    value = MAX_COLOR * (value * 0.01);

    for (int i = 0; i < fileStruct -> size_y; i++) {
        for (int j = 0; j < fileStruct -> size_x; j++) {
            if (data[i][j] <= value) {
                data[i][j] = MIN_COLOR;

            } else if (data[i][j] > value) {
                data[i][j] = data[i][j];

            } else {
                fprintf(stderr, "Error: Can not convent\n");
                exit(-1);
            }
        }
    }

    return 0;
}

int threshold_white(FileStruct *fileStruct, double value) {
    int (*data)[fileStruct -> size_x];
    data = (int(*)[fileStruct -> size_x]) fileStruct -> t_data;
    value = MAX_COLOR * (value * 0.01);

    for (int i = 0; i < fileStruct -> size_y; i++) {
        for (int j = 0; j < fileStruct -> size_x; j++) {
            if (data[i][j] <= value) {
                data[i][j] = MIN_COLOR;

            } else if (data[i][j] > value) {
                data[i][j] = MAX_COLOR;

            } else {
                fprintf(stderr, "Error: Can not convent\n");
                exit(-1);
            }
        }
    }

    return 0;
}

int c_gamma(FileStruct *fileStruct, double value) {
    int (*data)[fileStruct -> size_x];
    data = (int(*)[fileStruct -> size_x]) fileStruct -> t_data;

    for (int i = 0; i < fileStruct -> size_y; i++) {
        for (int j = 0; j < fileStruct -> size_x; j++) {
            data[i][j] = pow(data[i][j], (1.0 / value)) / pow(MAX_COLOR, ((1.0 - value) / value));
        }
    }

    return 0;
}

int kontur(FileStruct *fileStruct, double value) {
    int int_value = (int) value;
    int (*data)[fileStruct -> size_x];
    int (*data_processed)[fileStruct -> size_x];
    int *p_data = malloc(fileStruct -> size_x * fileStruct -> size_y * sizeof(int));

    data = (int(*)[fileStruct -> size_x]) fileStruct -> t_data;
    data_processed = (int(*)[fileStruct -> size_x]) p_data;

    for (int i = 0; i < fileStruct -> size_y; i++) {
        for (int j = 0; j < fileStruct -> size_x; j++) {
            if ((i > int_value && j > int_value) && (i < fileStruct -> size_y - int_value && j < fileStruct -> size_x - int_value)) {
                data_processed[i][j] = (int)(abs(data[i + int_value][j] - data[i][j]) + abs(data[i][j + int_value] - data[i][j]));
            } else {
                data_processed[i][j] = data[i][j];
            }
        }
    }

    free(data);
    fileStruct -> t_data = data_processed;
  
    return 0; 
}

int blur(FileStruct *fileStruct, double value) {
    int int_value = (int) value;
    int (*data)[fileStruct -> size_x];
    int (*data_processed)[fileStruct -> size_x];
    int *p_data = malloc(fileStruct -> size_x * fileStruct -> size_y * sizeof(int));

    data = (int(*)[fileStruct -> size_x]) fileStruct -> t_data;
    data_processed = (int(*)[fileStruct -> size_x]) p_data;

    for (int i = 0; i < fileStruct -> size_y; i++) {
        for (int j = 0; j < fileStruct -> size_x; j++) {
            if (i >= int_value && i < fileStruct -> size_y - int_value &&
            	j >= int_value && j < fileStruct -> size_x - int_value) {
                data_processed[i][j] = (int)
                (1.0/5.0 * 
                (data[i - int_value][j] + data[i][j] + data[i + int_value][j] + 
                data[i][j - int_value] 	+	data[i][j + int_value]));
            } else {
                data_processed[i][j] = data[i][j];
            }
        }
    }

    free(data);
    fileStruct -> t_data = data_processed;

    return 0;
}

int normalize(FileStruct *fileStruct) {
    int max_color = 125, min_color = 125, diff;
    int (*data)[fileStruct -> size_x];

    data = (int(*)[fileStruct -> size_x]) fileStruct -> t_data;

    for (int i = 0; i < fileStruct -> size_y; i++) {
        for (int j = 0; j < fileStruct -> size_x; j++) {
            if (data[i][j] > max_color)
                max_color = data[i][j];
            if (data[i][j] < min_color)
                min_color = data[i][j];
        }
    }

    diff = (int)(MAX_COLOR / (max_color - min_color));

    for (int i = 0; i < fileStruct -> size_y; i++) {
        for (int j = 0; j < fileStruct -> size_x; j++) {
            data[i][j] = (int)((data[i][j] - min_color) * diff);
        }
    }    

    return 0;
}

int convent(FileStruct *fileStruct) {
    fileStruct->size_y=fileStruct->size_y/3;

    int (*data)[fileStruct -> size_x];
    int (*data_processed)[fileStruct -> size_x];
    int *p_data = malloc(fileStruct -> size_x * fileStruct -> size_y * sizeof(int));

    data = (int(*)[fileStruct -> size_x]) fileStruct -> t_data;
    data_processed = (int(*)[fileStruct -> size_x]) p_data;

    for (int i = 0; i < fileStruct -> size_y; i++) {
        for (int j = 0; j < fileStruct -> size_x; j++) {
            data_processed[i][j] = data[i * 3][j * 3];
        }

    }

    free(data);
    fileStruct -> t_data = data_processed;
    fileStruct -> buffer[1] = '2';

    return 0;
}