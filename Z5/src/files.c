#include <stdio.h>
#include <stdlib.h>
#include <../lib/files.h>

FileStruct* init_pgm(FileStruct* fileStruct) {
    fileStruct = (FileStruct*)malloc(sizeof(FileStruct));

    return fileStruct;
}

int read(FILE *file_in, FileStruct* fileStruct) {
    alloc_buff(fileStruct);
    validate(file_in, fileStruct -> buffer);

    fscanf(file_in, "%d %d %d", &fileStruct -> size_x, &fileStruct -> size_y, &fileStruct -> grey_value);

    if (fileStruct->buffer[1] == '3') 
        fileStruct -> size_y = fileStruct -> size_y * 3;

    alloc_data(file_in, fileStruct);

    fclose(file_in);

    return 0; 
}

int write(FILE *file_out, FileStruct* fileStruct) {
    int (*data)[fileStruct -> size_x];
    data = (int(*)[fileStruct -> size_x]) fileStruct -> t_data;

    if (fileStruct->buffer[1] == '2') {
        fputs("P2\n", file_out);
        fprintf(file_out, "%d %d\n%d\n", fileStruct -> size_x, fileStruct -> size_y, fileStruct -> grey_value);

    } else {
        fputs("P3\n", file_out);
        fprintf(file_out, "%d %d\n%d\n", fileStruct -> size_x, (int)fileStruct -> size_y / 3, fileStruct -> grey_value);

    }

    for (int i = 0; i < fileStruct -> size_y; i++) {
        for (int j = 0; j < fileStruct -> size_x; j++) {
            fprintf(file_out, "%d ", data[i][j]);
        }
    }

    fclose(file_out);

    return 0; 
}