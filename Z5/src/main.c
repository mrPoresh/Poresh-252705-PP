#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <../lib/struct.h>
#include <../lib/files.h>
#include <../lib/filters.h>

#define W_OK 0                   /* wartosc oznaczajaca brak bledow */
#define B_NIEPOPRAWNAOPCJA -1    /* kody bledow rozpoznawania opcji */
#define B_BRAKNAZWY   -2
#define B_BRAKWARTOSCI  -3
#define B_BRAKPLIKU   -4

/* strukura do zapamietywania opcji podanych w wywolaniu programu */
typedef struct {

  FILE *file_in, *file_out;        /* uchwyty do pliku wej. i wyj. */
  int negatyw,progowanie,konturowanie,wyswietlenie;      /* opcje */
  int option_value;              /* wartosc progu dla opcji progowanie */ 

} OptionStruct;

/*******************************************************/
/* Funkcja inicjuje strukture wskazywana przez wybor   */
/* PRE:                                                */
/*      poprawnie zainicjowany argument wybor (!=NULL) */
/* POST:                                               */
/*      "wyzerowana" struktura wybor wybranych opcji   */
/*******************************************************/

void wyzeruj_opcje(OptionStruct * optionsParam) {
  optionsParam -> file_in=NULL;
  optionsParam -> file_out=NULL;
  optionsParam -> negatyw=0;
  optionsParam -> konturowanie=0;
  optionsParam -> progowanie=0;
  optionsParam -> wyswietlenie=0;
  optionsParam -> option_value=0;
}

void draw(char* file) {
    char buf[1024];

    strcpy(buf, "feh ");
    strcat(buf, file);
    printf("%s\n", buf);
    system(buf);
}

/************************************************************************/
/* Funkcja rozpoznaje opcje wywolania programu zapisane w tablicy argv  */
/* i zapisuje je w strukturze wybor                                     */
/* Skladnia opcji wywolania programu                                    */
/*         program {[-i nazwa] [-o nazwa] [-p liczba] [-n] [-r] [-d] }  */
/* Argumenty funkcji:                                                   */
/*         argc  -  liczba argumentow wywolania wraz z nazwa programu   */
/*         argv  -  tablica argumentow wywolania                        */
/*         wybor -  struktura z informacjami o wywolanych opcjach       */
/* PRE:                                                                 */
/*      brak                                                            */
/* POST:                                                                */
/*      funkcja otwiera odpowiednie pliki, zwraca uchwyty do nich       */
/*      w strukturze wybór, do tego ustawia na 1 pola dla opcji, ktore  */
/*	poprawnie wystapily w linii wywolania programu,                 */
/*	pola opcji nie wystepujacych w wywolaniu ustawione sa na 0;     */
/*	zwraca wartosc W_OK (0), gdy wywolanie bylo poprawne            */
/*	lub kod bledu zdefiniowany stalymi B_* (<0)                     */
/* UWAGA:                                                               */
/*      funkcja nie sprawdza istnienia i praw dostepu do plikow         */
/*      w takich przypadkach zwracane uchwyty maja wartosc NULL         */
/************************************************************************/

int przetwarzaj_opcje(int argc, char **argv, OptionStruct *optionsParam) {
    int i;
    char *nazwa_pliku_we, *nazwa_pliku_wy;
    double value;



    wyzeruj_opcje(optionsParam);
    optionsParam->file_out=stdout;        /* na wypadek gdy nie podano opcji "-o" */

    for (i=1; i<argc; i++) {

        if (argv[i][0] != '-')  /* blad: to nie jest opcja - brak znaku "-" */
            return B_NIEPOPRAWNAOPCJA;

        switch (argv[i][1]) {

            case 'i': {                 /* opcja z nazwa pliku wejsciowego */
                
                if (++i<argc) {   /* wczytujemy kolejny argument jako nazwe pliku */
                    nazwa_pliku_we=argv[i];
	
                    if (strcmp(nazwa_pliku_we,"-")==0) /* gdy nazwa jest "-"        */
	                    optionsParam->file_in=stdin;            /* ustwiamy wejscie na stdin */
	            
                    else                               /* otwieramy wskazany plik   */
	                    optionsParam->file_in=fopen(nazwa_pliku_we,"r");
                
                } else 
	                return B_BRAKNAZWY;                   /* blad: brak nazwy pliku */
            
                break;
            
            }
    
            case 'o': {                 /* opcja z nazwa pliku wyjsciowego */
                
                if (++i<argc) {   /* wczytujemy kolejny argument jako nazwe pliku */
	                nazwa_pliku_wy=argv[i];
	            
                    if (strcmp(nazwa_pliku_wy,"-")==0)/* gdy nazwa jest "-"         */
	                    optionsParam->file_out=stdout;          /* ustwiamy wyjscie na stdout */
	            
                    else                              /* otwieramy wskazany plik    */
	                    optionsParam->file_out=fopen(nazwa_pliku_wy,"w");
                } else 
	                return B_BRAKNAZWY;                   /* blad: brak nazwy pliku */
                    
                break;
            }

            case 't': {

                if (++i<argc) {

                    FileStruct *fileStruct = init_pgm(fileStruct);
                    read(optionsParam -> file_in, fileStruct);

                    switch (argv[i][0]) {

                        case 'd': {

                            if (++i<argc) {
                                if (sscanf(argv[i], "%le", &value) == 1) {
	                                optionsParam -> progowanie = 1;
	                                optionsParam -> option_value = value;

                                    threshold_default(fileStruct, optionsParam -> option_value);
	                
                                } else
	                                return B_BRAKWARTOSCI;
                                
                            }

                            break;
                        }

                        case 'b': {

                            if (++i<argc) {
                                if (sscanf(argv[i], "%le", &value) == 1) {
	                                optionsParam -> progowanie = 1;
	                                optionsParam -> option_value = value;

                                    threshold_black(fileStruct, optionsParam -> option_value);
	                
                                } else
	                                return B_BRAKWARTOSCI;
                                
                            }

                            break;
                        }

                        case 'w': {
                            if (++i<argc) {
                                if (sscanf(argv[i], "%le", &value) == 1) {
	                                optionsParam -> progowanie = 1;
	                                optionsParam -> option_value = value;

                                    threshold_white(fileStruct, optionsParam -> option_value);
	                
                                } else
	                                return B_BRAKWARTOSCI;
                                
                            }

                            break;
                        }
                    }    

                    write(optionsParam -> file_out, fileStruct);
                    draw(nazwa_pliku_wy);

                    free_memory(fileStruct);
                
                } else 
	                return B_BRAKWARTOSCI;             
                
                break;
            }

            case 'g': {

                if (++i<argc) { /* wczytujemy kolejny argument jako wartosc progu */
	                if (sscanf(argv[i], "%le", &value)==1) {
	                    optionsParam -> progowanie=1;
	                    optionsParam -> option_value=value;

                        FileStruct *fileStruct = init_pgm(fileStruct);

                        read(optionsParam -> file_in, fileStruct);
                        c_gamma(fileStruct, optionsParam -> option_value);
                        write(optionsParam -> file_out, fileStruct);

                        draw(nazwa_pliku_wy);

                        free_memory(fileStruct);
	                
                    } else
	                    return B_BRAKWARTOSCI;     /* blad: niepoprawna wartosc progu */
                
                } else 
	                return B_BRAKWARTOSCI;             /* blad: brak wartosci progu */
                
                break;
            }

            case 'n': {                 /* mamy wykonac negatyw */
      
                optionsParam->negatyw=1;

                FileStruct *fileStruct = init_pgm(fileStruct);

                read(optionsParam -> file_in, fileStruct);
                negatiwe(fileStruct);
                write(optionsParam -> file_out, fileStruct);

                draw(nazwa_pliku_wy);

                free_memory(fileStruct);
                
                break;
            }

            case 'k': {

                if (++i<argc) { /* wczytujemy kolejny argument jako wartosc progu */
	                if (sscanf(argv[i], "%le", &value)==1) {
	                    //optionsParam -> progowanie=1;
	                    optionsParam -> option_value=value;

                        FileStruct *fileStruct = init_pgm(fileStruct);

                        read(optionsParam -> file_in, fileStruct);
                        kontur(fileStruct, optionsParam -> option_value);
                        write(optionsParam -> file_out, fileStruct);

                        draw(nazwa_pliku_wy);

                        free_memory(fileStruct);
	                
                    } else
	                    return B_BRAKWARTOSCI;     /* blad: niepoprawna wartosc progu */
                
                } else 
	                return B_BRAKWARTOSCI;             /* blad: brak wartosci progu */
                
                break;
            }

            case 'b': {

                if (++i<argc) { /* wczytujemy kolejny argument jako wartosc progu */
	                if (sscanf(argv[i], "%le", &value)==1) {
	                    //optionsParam -> progowanie=1;
	                    optionsParam -> option_value=value;

                        FileStruct *fileStruct = init_pgm(fileStruct);

                        read(optionsParam -> file_in, fileStruct);
                        blur(fileStruct, optionsParam -> option_value);
                        write(optionsParam -> file_out, fileStruct);

                        draw(nazwa_pliku_wy);

                        free_memory(fileStruct);
	                
                    } else
	                    return B_BRAKWARTOSCI;     /* blad: niepoprawna wartosc progu */
                
                } else 
	                return B_BRAKWARTOSCI;             /* blad: brak wartosci progu */
                
                break;
            }
    
            case 'r': {                 /* mamy wyswietlic obraz */
      
                optionsParam->wyswietlenie=1;

                FileStruct *fileStruct = init_pgm(fileStruct);

                read(optionsParam -> file_in, fileStruct);
                normalize(fileStruct);
                write(optionsParam -> file_out, fileStruct);

                draw(nazwa_pliku_wy);

                free_memory(fileStruct);
                
                break;
            }

            case 'd': {                 /* mamy wyswietlic obraz */
      
                optionsParam->wyswietlenie=1;

                FileStruct *fileStruct = init_pgm(fileStruct);

                read(optionsParam -> file_in, fileStruct);
                write(optionsParam -> file_out, fileStruct);

                draw(nazwa_pliku_wy);

                free_memory(fileStruct);
                
                break;
            }

            case 'c': {                 /* mamy wyswietlic obraz */
      
                //optionsParam->wyswietlenie=1;

                FileStruct *fileStruct = init_pgm(fileStruct);

                read(optionsParam -> file_in, fileStruct);
                convent(fileStruct);
                write(optionsParam -> file_out, fileStruct);

                draw(nazwa_pliku_wy);

                free_memory(fileStruct);
                
                break;
            }

            default:                    /* nierozpoznana opcja */
                return B_NIEPOPRAWNAOPCJA;

        } /* koniec switch */
    } /* koniec for */

  if (optionsParam->file_in!=NULL)     /* ok: wej. strumien danych zainicjowany */
    return W_OK;
  else 
    return B_BRAKPLIKU;         /* blad:  nie otwarto pliku wejsciowego  */

}

/*******************************************************/
/* Testowe wywołanie funkcji przetwarzaj_opcje         */
/* PRE:                                                */
/*      brak                                           */
/* POST:                                               */
/*      brak                                           */
/*******************************************************/

int main(int argc, char ** argv) {
    OptionStruct optionsParam;
    int error_code;
  
    error_code = przetwarzaj_opcje(argc,argv,&optionsParam);

    if (error_code)
        printf("Error nr %d\n", error_code);
    else
        printf("Options are good\n");

    return error_code;
}