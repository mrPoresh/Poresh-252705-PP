#include <stdio.h>
#include <stdlib.h>
#include <../lib/struct.h>


int validate(FILE *file, char *buffer) {
    validate_comments(file, buffer);
    validate_head(file, buffer);

    return 0; 
}

int validate_head(FILE *file, char *buffer) {
    if (file == NULL) {
        fprintf(stderr, "Error: File does not exist\n");
        exit(-1);
    }

    if (fgets(buffer, BUFF_SIZE, file) == NULL) {
        fprintf(stderr, "Error: Is not PGM\n");
        exit(-1);
    }

    if ((buffer[0] != 'P') && ((buffer[1] != '2') || (buffer[1] != '3'))) {
        fprintf(stderr,"Error: Is not PGM\n");
        exit(-1);
    }

    return 0;
}

int validate_comments(FILE *file, char *buffer) {
    int value, end = 0;

    do {
        if ((value=fgetc(file))=='#') {
            if (fgets(buffer, BUFF_SIZE, file)==NULL)
                end = 1;
        } else {
            ungetc(value,file);
        }

    } while (value == '#' && !end);

    return 0;
}