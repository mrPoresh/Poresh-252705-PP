#include <stdio.h>
#include <stdlib.h>
#include <../lib/struct.h>


int alloc_buff(FileStruct *fileStruct) {
    fileStruct -> buffer = (char* )calloc(BUFF_SIZE, sizeof(char));

    return 0;
}

int alloc_data(FILE *file, FileStruct *fileStruct) {
    fileStruct -> t_data = malloc(fileStruct -> size_x * fileStruct -> size_y * sizeof(int));

    int (*data)[fileStruct -> size_x];
    data = (int(*)[fileStruct -> size_x]) fileStruct -> t_data;

    for (int i = 0; i < fileStruct -> size_y; i++) {
        for (int j = 0; j < fileStruct -> size_x; j++) {
            fscanf(file, "%d", &(data[i][j]));
        }
    }

    return 0;
}

int free_memory(FileStruct* fileStruct) {

    free(fileStruct -> buffer);
    free(fileStruct -> t_data);
    free(fileStruct);

    return 0;
}