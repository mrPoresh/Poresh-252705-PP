#include <stdio.h>
#include <stdbool.h>
#include <math.h>


void PrintHeadMenu();
void PrintOptions();
void Trojmian(float, float, float);
void TestCase();


int main() {

    bool status = true;
    float a, b, c;
    int option;

    PrintHeadMenu();

    do {

        PrintOptions();

        scanf("%d", &option);

        switch(option) {

            case 1:

                printf("Podaj wartosc a:");
                scanf("%f",&a);
                printf("Podaj wartosc b:");
                scanf("%f",&b);
                printf("Podaj wartosc c:");
                scanf("%f",&c);
  
                Trojmian(a, b, c);

                break;

            case 2:

                TestCase();

                break;

            case 3:

                status = false;

                break;

        }

    } while (status == true);

}


void Trojmian(float a, float b, float c) {

    float delta;

    if (a==0.0) {   /*przypadek rownania liniowego */

        if (b!=0.0) {

            printf("| Jest to równanie liniowe o rozwiazaniu x=%f\n",-c/b);///!!! sprawozdanie Latex check !!! 

        } else  if (c==0.0) { /* oraz a==b==0.0 */

            printf("| Rozwiazaniem jest dowolne x\n");

        } else              /* a==b==0.0 != c  */

            printf("| Brak rozwiazan\n");

    } else {   /*przypadek rownania kwadratowego */

        delta=pow(b,2)-4.0*a*c;

        printf("| Delta is ->%.6f\n",delta);   //6 digit

        if (delta<0) {

            printf("| Brak rozwiazan rzeczywistych\n");

        } else          /* delta>=0 */

            if (delta>0) {

	            printf("| Rozwiazaniem sa x1=%f i x2=%f\n",(-b-sqrt(delta))/(2*a),(-b+sqrt(delta))/(2*a));

            } else

      	        printf("| Rozwiazaniem sa x1=x2=%f\n",-b/(2*a));  //delta = 0

    }

}

void TestCase() {

    float a, b, c;

    printf("*                                              *\n"
            "|              Start Test Case                 |\n"
            "|                                              |\n"
            "* Test Case 1 -> if a==0 b==0 c==0             *\n");

            Trojmian(a=0.0, b=0.0, c=0.0);

    printf("* Test Case 2 -> if a==-1 b==0 c==0            *\n");

            Trojmian(a=-1.0, b=0.0, c=0.0);

    printf("* Test Case 3 -> if a==0 b==-1 c==0            *\n");

            Trojmian(a=0.0, b=-1.0, c=0.0);

    printf("* Test Case 4 -> if a==0 b==0 c==-1            *\n");

            Trojmian(a=0.0, b=0.0, c=-1.0);

    printf("* Test Case 5 -> if a==0.00..1 b==1 c==1       *\n");

            Trojmian(a=0.0000000000000000000000001, b=1.0, c=1.0);

    printf("* Test Case 6 -> if a==0 b==0.00..1 c==99999.. *\n");

            Trojmian(a=0.0, b=0.0000000000000000000000001, c=9999999999999999999999999999.9);

    printf("*______________________________________________*\n");

}

void PrintHeadMenu() {

    printf("*                                              *\n"
            "|Program oblicza pierwiastki rownania w postaci|\n"
            "|     2                                        |\n"
            "|  a x + b x +c =0                             |\n"
            "*                                              *\n");

}

void PrintOptions() {

    printf("*                                              *\n"
            "|                   Options                    |\n"
            "|                                              |\n"
            "| 1 -> Start main program                      |\n"
            "| 2 -> Start test case                         |\n"
            "| 3 -> Stop program                            |\n"
            "|                                              |\n"
            "*                                              *\n");

}