#!/bin/bash

cd .. 
cd DataStorage

read -p "Enter the path to file: " path
cd $path

read -p "Enter the filename: " file

read -p "Enter the search string: " word
read -p "Enter the replace string: " new_word

sed -i "s/$word/$new_word/g" $file