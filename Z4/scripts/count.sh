#!/bin/bash

# skrypt informujacy o sumarycznej wielkosci i liczbie plikow w
# biezacym katalogu 
cd .. 
cd DataStorage

echo -n "Summary file Size: "
du -sh
echo -n "Total files: "

ls -lR | grep ^- | wc -l