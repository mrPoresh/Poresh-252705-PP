#!/bin/bash

cd .. 
cd DataStorage

read -p "Enter the path to file: " path
cd $path

read -p "Enter the original filename to rename: " name
read -p "Enter the renamed filename to rename: " new_name

if [ -f $name ]; then
    $(mv $name $new_name)
    echo "The file is renamed."
fi