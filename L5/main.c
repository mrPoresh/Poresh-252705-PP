#include <stdio.h>
#include <math.h>
#include <time.h>

struct Person {
    char name[50];
    int citNo;
    float salary;
};

struct Compare {
    int (*is_less)(int, int, void*);
    double (*array_value)(int, void*);
    void* array;
};

void printArray(int arr[], int n);
void swapPointer(int *xp, int *yp);
int partition(int array[], int low, int high, struct Compare* cmp);
void merge(int arr[], int start_index, int half_index, int end_index, struct Compare* cmp);

int cmp_less_person(int index_left, int index_right, void* array) {
    struct Person* person_array = (struct Person*)(array);
    return person_array[index_left].salary < person_array[index_right].salary;
}

double cmp_value_person(int index, void* array) {
    struct Person* person_array = (struct Person*)(array);
    return person_array[index].salary;
}

/* Insertion Sort (Sortowanie przez proste wstawianie) */
void insertionSort(int arr[], int size, struct Compare* cmp) {  // array & size of array
    int i, j, temp;

    for (i = 1; i < size; i++) {   // Sorting the entire array starting from the 2nd element
        temp = arr[i];           
        j = i - 1;              // Index for the previous value

        while (j >= 0) {    // While current value is less then previous
            if (cmp) {
            	if (!cmp -> is_less(temp, arr[j], cmp -> array))
            	    break;
            }
            else if (!(temp < arr[j]))
                break;
            
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = temp;
    }
}

/* Bubble sort (Sortowanie bąbelkowe) 
/
/   Working of Bubble Sort:
/
/   1 -> First Iteration (Compare and Swap)
/       - Starting from the first index, compare the first and the second elements.
/       - If the first element is greater than the second element, they are swapped and etc.
/       - The above process goes on until the last element.
/
/   The same process goes on for the remaining iterations.
/   After each iteration, the largest element among the unsorted elements is placed at the end.
/
/   2 -> Remaining Iteration
/       - In each iteration, the comparison takes place up to the last unsorted element.
/       - The array is sorted when all the unsorted elements are placed at their correct positions.
/
*/
void bubbleSort(int arr[], int size, struct Compare* cmp) {  // array & size of array
    int i, j, swapped;

    for (i = 0; i < size; i++) {    // Loop for access each array element
        swapped = 0;                     

        for (j = 0; j < size - i; j++) {
            if ((arr[j] > arr[j+1]) && !cmp) {    // If the next one is smaller than the current
                swapPointer(&arr[j], &arr[j+1]);    // Swap

                swapped = 1;
            }
            if (cmp) {
                if (cmp -> is_less(arr[j + 1], arr[j], cmp -> array)) {
            	    swapPointer(&arr[j], &arr[j+1]);
                    swapped = 1;
                }
            }
        }

        if (swapped == 0) {   // If swapped == 0 it means that array is already sorted
            break;
        }
    }

}

/* Quick Sort ()
/
/    Quicksort is a sorting algorithm based on the divide and conquer approach where:
/    
/       1 -> An array is divided into subarrays by selecting a pivot element (element selected from the array).
/       2 -> The left and right subarrays are also divided using the same approach. 
/                This process continues until each subarray contains a single element.
/       3 -> At this point, elements are already sorted. Finally, elements are combined to form a sorted array.
/
*/
void quickSort(int arr[], int low, int high, struct Compare* cmp) {
    if (low < high) {
        int pi = partition(arr, low, high, cmp); // Find the pivot element

        quickSort(arr, low, pi - 1, cmp);        // Recursive call on the left of pivot
        quickSort(arr, pi + 1, high, cmp);       // Recursive call on the right of pivot
    }
}

/* */
void mergeSort(int arr[], int start_index, int end_index, struct Compare* cmp) {
    if (start_index < end_index) {
        int half_index = start_index + (end_index - start_index) / 2;
        //printf("Half %d \n", half_index);

        mergeSort(arr, start_index, half_index, cmp);
        mergeSort(arr, half_index + 1, end_index, cmp);

        merge(arr, start_index, half_index, end_index, cmp);
    }
}

/* */
void merge(int arr[], int start_index, int half_index, int end_index, struct Compare* cmp) {
    int i, j, k;
    int len1 = half_index - start_index + 1;    // = 5 - 0 + 1 = 6 => [0..5]
    int len2 = end_index - half_index;          // = 9 - 5     = 4 => [6..9]
//                                              //  _______________________                                              
    int sub_arr1[len1], sub_arr2[len2];         // |

    for (i = 0; i < len1; i++) {                // |
        sub_arr1[i] = arr[start_index + i];     // |
    }                                           // | -> Creating dublicate copies of sub-arrays

    for (j = 0; j < len2; j++) {                // |
        sub_arr2[j] = arr[half_index + 1 + j];  // |
    }                                           // |

    i = 0;              // sub1 |
    j = 0;              // sub2 | -> Current index of sub-arrays and main array
    k = start_index;    // main |

    while (i < len1 && j < len2) {
        if (cmp) {
            if (cmp -> array_value(sub_arr1[i], cmp -> array) <= cmp -> array_value(sub_arr2[j], cmp -> array)) {
                arr[k] = sub_arr1[i]; i++;
            } else {
                arr[k] = sub_arr2[j]; j++;
            }
            k++;

        } else {
            if (sub_arr1[i] <= sub_arr2[j]) {
                arr[k] = sub_arr1[i]; i++;

            } else {
                arr[k] = sub_arr2[j]; j++;
            }
            k++;
        }
    }

    while (i < len1) {
        arr[k] = sub_arr1[i];
        i++;
        k++;
    }

    while (j < len2) {
        arr[k] = sub_arr2[j];
        j++;
        k++;
    }
}

/* For find the partition position*/
int partition(int arr[], int low, int high, struct Compare* cmp) {
    int pivot;  
    int i = (low - 1);      // Pointer for greater element
    int value;

    if (cmp) {                                                  // Selecting the rightmost element as pivot
        pivot = cmp -> array_value(arr[high], cmp -> array);

    } else {
        pivot = arr[high];
    }

    for (int j = low; j < high; j++) {  // Loop for access each array element
        if (cmp) {
            value = cmp -> array_value(arr[j], cmp -> array);
            
            if (value <= pivot) {
                i++;
                swapPointer(&arr[i], &arr[j]);
            }
            value = 0;
        }
        else if (arr[j] <= pivot) {          // and compare them with the pivot
            i++;                                // if element smaller than pivot is found
/*     */   swapPointer(&arr[i], &arr[j]);      // swap it with the greater element pointed by i
        }
    }

    swapPointer(&arr[i + 1], &arr[high]);       // Swap the pivot element with the greater element at i       

    return (i + 1);         // return the partition point
} 

/* For swaping pointers */
void swapPointer(int *xp, int *yp) {
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

/* For printing arrays */ 
void printArray(int arr[], int n) {
    int i;

    for (i = 0; i < n; i++)
        printf("%d ", arr[i]);
    printf("\n");
}

int main() {
    char option;
    clock_t time;

    int arr[] = { 12, 11, 13, 5, 6, 1, 5, 3, 2, 0, 7, 4 };
    int indices[] = { 0, 1, 2, 3, 4, 5 };
    int size = sizeof(arr) / sizeof(arr[0]);

    struct Compare cmp;
    struct Person persons[] = {{"Anon 1", 1, 3.0f}, {"Anon 2", 1, 5.0f}, {"Anon 3", 1, 2.0f},
                               {"Anon 4", 1, 2.5f}, {"Anon 5", 1, 1.2f}, {"Anon 6", 1, 3.8f}};
    // If will be sorted by salary, the correct result is -> 4 2 3 0 5 1

    cmp.is_less = cmp_less_person;
    cmp.array_value = cmp_value_person;
    cmp.array = persons;

    printf("Default Array: ");
    printArray(arr, size);
    printf("\nStruct  Array: ");
    printArray(indices, 6);
    printf("\nGive type of sorting:\n- I -> Insertion Sorting\n- B -> Bubble Sorting\n- Q -> Qick Sorting\n- M -> Merge Sorting\n\n");
    scanf("%c", &option);

    time = clock();

    switch (option) {

        case 'I':
            insertionSort(arr, size, 0);
            insertionSort(indices, 6, &cmp);
            break;

        case 'B':
            bubbleSort(arr, size - 1, 0);
            bubbleSort(indices, 6 - 1, &cmp);
            break;

        case 'Q':
            quickSort(arr, 0, size - 1, 0);
            quickSort(indices, 0, 6 - 1, &cmp);
            break;

        case 'M':
            mergeSort(arr, 0, size - 1, 0);
            mergeSort(indices, 0, 6 - 1, &cmp);
            break;

    }

    time = clock() - time;
    double time_taken = ((double) time) / CLOCKS_PER_SEC;

    printf("\nSorted Array: ");
    printArray(arr, size);
    printf("\nSorted Array: ");
    printArray(indices, 6);
    
    printf("\nTime of execute (in sec): %f\n\n", time_taken);

    return 0;
}