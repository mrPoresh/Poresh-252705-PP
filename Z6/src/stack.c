#include <stdio.h>
#include <stdlib.h>

#include <../lib/stack.h>


void init(struct node* head) {
    head = NULL;
}

void display(struct node* head) {
    struct node *current = head;

    if(current != NULL) {
        printf("Stack: ");
        
        do {

            printf("%d ", current -> data);
            current = current -> next;

        } while (current != NULL);
        printf("\n");

    } else {
        printf("The Stack is empty\n");
    }

}

void top(struct node* head) {
    if(head != NULL){
        printf("Element on top: %d\n", head -> data);

    } else {
        printf("The stack is empty.\n");
    }
}

int get(struct node* head) {
    if (head != NULL) {
        if (&head -> data != NULL) {
            return head -> data;
        } else {
            return head -> data = 0;
        }
    } else {
        head = (struct node*)malloc(sizeof(struct node));
        return head -> data = 0;
    }
}

//                                                                  //    head
struct node* push(struct node* head, int data) {                    //   __________       ___________
    struct node* tmp = (struct node*)malloc(sizeof(struct node));   //  |   2   |  | --> |    1   |  | --> NULL
    if(tmp == NULL) {                                               //  |_______|__|     |________|__|    
        exit(0);
    }

    tmp->data = data;
    tmp->next = head;
    head = tmp;

    return head;
}

struct node* pop(struct node *head) {
    if (head != NULL) {
        struct node* tmp = head;
        head = head->next;
        free(tmp);

    } else {
        printf("The stack is empty.\n");
    }

    return head;
}

struct node* revers(struct node* head) {
    struct node* tmp = NULL;

    while (head != NULL) {
        tmp = push(tmp, head -> data);
        head = pop(head);
    }

    return tmp;
}