#ifndef LINKEDSTACK_H_INCLUDED
#define LINKEDSTACK_H_INCLUDED


struct node{
    int data;
    struct node* next;
};

void init(struct node*);
void display(struct node*);

int empty(struct node*);

struct node* push(struct node*, int);
struct node* pop(struct node* , int*);

#endif