#include <stdio.h>
#include <stdlib.h>

#include <../lib/stack.h>


int main() {

    char option;
    int in_value, out_value = 0;
    char input_str[32];

    struct node *head = NULL;

    init(head);

    do {

	    scanf("%s", input_str);
	
        if (sscanf(input_str, "%d", &in_value)) {             
            head = push(head, in_value);

        } else {
            sscanf(input_str, "%c", &option);

            if ((option != 'd') && (option != 'c') && (option != 'r')) {
                out_value = get(head);
	            head = pop(head);
            }
            
            switch (option) {

                case '+':                                        
			        out_value = get(head) + out_value;                                                          
                break;

                case '-':
			        out_value = get(head) - out_value;    
                break;
                
                case '*':
			        out_value = get(head) * out_value;    
                break;

		        case '/':
                    if (out_value != 0) {
			            out_value = get(head) / out_value;
                    } else {
                        out_value = get(head);
                        printf("Error: You can't Devide by zero.\n");
                    }    
                break;

                case 'd':
                    //:)
                break;

                case 'c':
                    do {
                        head = pop(head);
                    } while (head != NULL);
                break;

                case 'r':
                    head = revers(head);
                break;
            }

            if ((option != 'd') && (option != 'c') && (option != 'r')) {
                head = pop(head);
                head = push(head, out_value);

                printf("Value is: %d\n", out_value);
                out_value = 0;
            
                //display(head);
            }

            display(head);

        }

    } while (option != 'q');

    return 0;
}